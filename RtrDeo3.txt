Proposed RTR District Education Officer
( हिंदी अनुवाद अंग्रेजी ड्राफ्ट बाद दिया है )
राईट टू रिकॉल जिला शिक्षा अधिकारी का ड्राफ्ट 
-------------------------
.
[ comment : This RTR-DEO law-draft can be printed by any Chief Minister in the State Govt’s Gazette. The CM would not need permission of Assembly or Parliament.
.
This draft has 3 parts - 
.
Part - I : Basic instructions for citizens
. 
Part - II : Summary of the procedure given in the draft 
.
Part - III : Instructions for Officers and comments 
.
Important clauses are – (1.2) , (1.3) , (1.5) , (8) , (10) . In the opinion of authors of this law-draft, other clauses are not critical and mere common sense. ]
.
----
.
--------------------------------------------------
Part - I : Basic instructions for citizens
--------------------------------------------------
.
(1) Approving DEO candidate, or approving existing DEO :
.
(1.1) If a parent (i.e. a voter who has child below age of 17 years) wishes to replace DEO of his District, then that parent can visit office of Patwari (or Talati or Village Officer), any day to approve a candidate. There will be no specific date, time duration or time limit.
.
(1.2) At Patwari’s office, the parent will need to pay Rs 3 fee. And then parent can approve at most five candidates who wish to be DEO. The citizen may also approve existing DEO if he wishes existing DEO to continue, for free. The approvals will come on CM’s website and will become visible to all. So approval filing is open and not confidential. 
.
(1.2.a) Fee will be Re 1 for BPL card holder.
.
(1.2.b) comment : if a parent is approving existing DEO, then he can approve 4 other candidates at most. If the parent isnt approving existing DEO, then he can approve 5 candidates at most.
.
(1.3) The citizen can cancel the approvals of any candidate he has approved any day by visiting office of Patwari.
.
(1.4) A citizen can approve or not approve a candidate – there will be no ranking 
.
(1.5) The approval will remain valid till citizen cancels it, or when citizen’s name is removed from the voter list.
.
(1.6) The citizen may file / cancel his approval via ATM like device at Patwari’s office, bank ATM, SMS or mobile app, as and when CM makes this facility available.
.
(2) Later addition of Range Voting System : In future, the CM may or need not set up an additional system where in a parent may give marks to candidate between -100 (minus hundred) and 100. When such system comes, a parent may give marks to all or some of the candidates. Not giving marks to a candidate will be taken as giving zero marks to him. If a parent finds range voting system complicated, then he may continue to use approvals only.
.
--------------------------------------------------------------------
Part - II : Summary of the procedure give in the draft 
-----------------------------------------------------------------------
.
(3) comment : summary of whole law-draft
.
(3.1) Any citizen can approve upto 5 candidates, and may also approve existing DEO, anyday and cancel approvals anyday
.
(3.2) When approval of any candidate exceeds approvals of DEO AND approval of that candidate is over 35% of ALL parents in that district and is 2% more than approvals of existing DEO, then the CM may or need not appoint that candidate as new DEO. The decision of CM will be final.
.
(3.3) The voters of State can suspend RTR DEO procedure in a district for 4 years using explicit majority of voters. And the voters of India can suspend RTR DEO procedure in any District / State using explicit majority of voters.
.
----------------------------------------------------------------
Part –III : Instructions for Officers and comments
----------------------------------------------------------------
.
(4) what is comment : The comments are not part of actual code of this legislation draft. The activists and voters may use comments to understand the draft. And Jurors may use comments as a reference to decide a case. But the comments are NOT binding on anyone. 
.
.
(5) (5.1) The word parent here would mean a registered voter in a District who has one child between the age of 0 and 17 years. In RTR-DEO, only parent can file approval.
.
(5.2) The DC (DC = District Collector) would prepare and maintain the list of parents in District using guidelines made by CM. Till the list is made, any voter between the age of 23 years and 45 years will be taken as a parent.
.
(5.3) The word “may” does not imply any moral-legal binding. It clearly means “may” or “need not”. 
.
(6) Candidature for DEO
.
(6.1) If any person has a graduation degree, and is eligible to contest Loksabha election, then he is eligible to become DEO. If such a person comes to DC and wishes to be registered as a replacement of existing DEO, then the DC shall accept form, with a fee equal to deposit of MP election
.
(6.2) The form that the candidate will need to fill will be same as Loksabha form. DC will verify the form within 7 days and accept or reject it. 
.
(6.3) If form is rejected then Rs 2000 will be deducted and remaining deposit will be refunded. The candidate may submit form again.
.
(6.4) Upon acceptance, DC will issue a serial number and post replacement candidate’s name and details on CM’s website. The candidates can get their names removed by applying in writing.
.
(6.5) DC will register existing DEO, as candidate for free. The serial number of existing DEO will be 1.
.
(7) Citizen approving replacement candidate or existing DEO
.
(7.1) If a citizen comes in person to Patwari’s office, pays fee as decided by CM, and approves at most five replacement candidates for DEO’s position and / or if he approves existing DEO, then the Patwari would enter his approvals in the computer and would give receipt showing voter’s voter-id#, date/time and the candidates he approved. 
.
(7.2) If a parent comes to cancel his Approvals, Patwari will cancel one of more of his approvals without any fee. Cancellations will also come on CM’s website.
.
(7.3) comment : to explain approval – (i) The voter can approve anyday, and there is no time limit. And different voters can file approval anyday they like – not necessarily on same day or during any specific time interval. (ii) say a voter approves candidates A , B and C. Then approval counts of A, B and C will increase by 1. (iii) now anyday, voter may cancel any of the approval he has filed. Say he cancels approval of C. Then approval count of C will decrease by 1 and approval count of A and B will remain unchanged (iv) the voter can also approve any other candidate anyday. Say he approves D, in which case, approval count of D will increase by 1, and approval count of A and B will remain unchanged
.
(7.4) The Patwari will put approvals given by every citizen on CM’s website with citizen’s voter#, name and names of candidates he approved.

(7.5) CM may create a system which gives SMS notification to citizen on candidates he approved.
.
(7.5.a) comment : SMS mentioned above can be similar to SMS one gets when he makes a credit card transaction. So if someone is impostering a voter, then voter will come to know immediately and can work to register complaint. Also, finger print and photo will reduce the chance of impostering.

(7.6) CM may provide equipment to Patwari to capture photo and finger print of citizen, and can give receipt with the citizen’s finger print and photo. 
.
(7.7) CM may create a system where in citizen can submit/change his approvals over ATM type machines placed at Patwari office, or via SMS from registered mobile or via internet or via mobile phone apps. CM will decide the charges. CM may work with banks to enable citizens to register approvals via existing ATMs).
.
(7.8) Approval filing is open and public i.e. each voter’s approval of candidates will come on CM’s website in a way anyone in world can see 
.
(7.9) An approval filed by citizen will remain valid till citizen changes approval or citizen’s name is removed from voter list. So approval count of candidate will increase when a citizen approves and will decrease only when a citizen cancels approval or citizen’s name is removed from voter list.
.
(8) Condition for replacement of DEO : if
.
(8.1) a replacement candidate gets approval count over 35% of all (35% of ALL) parents in District AND
.
(8.2) his approval count is 2% (2% of all parents) more than approval count of existing DEO AND
.
(8.3) his approval count is highest of all
.
then, and then only, CM may or need not, remove existing DEO and appoint that candidate as the DEO.
.
(9) comment : 
.
(9.1) The “35%” and “2%” mentioned in clause above is “35% and 2% of all parents in the parent list” respectively. e.g. if a district has say 20 lakh parents, then 35% and 2% would mean 700,000 and 40,000 respectively regardless of number of parents who have filed approval. And all three conditions, (8.1) , (8.2) and (8.3) should be fulfilled.
.
(9.2) Example - 1: Say a District has 20,00,000 parents. Say existing DEO has approval of say 500,000 parents. Then if one candidate has approval of 35% of 20,00,000 = 700,000 parents, then CM may or need not appoint him as DEO. 
.
(9.3) Example - 2 : Say District has 20,00,000 parents. And say existing DEO has approval of say 800,000 parents. Then a candidate must have approval of (800,000 + 2% of 20,00,000) = (800,000 + 40,000) = 840,000 to for CM to appoint him as DEO. 
.
(10) One person can become DEO of upto 5 districts in a State and upto 15 Districts across India. If he has become DEO via approvals of parents i.e. in each district he has approval of over 35% of all parents, then his salary and perks will be proportional to number of district he is DEO of. E.g. if he is DEO of say 7 districts in India, and he has approval of over 35% parents in 5 districts, then his salary will be five times the salary of DEO. But if a person is DEO of say 5 districts and in each District he has approval of less than 35% parents in 4 or all 5 districts, then his salary will be of one DEO only.
.
(11) Term limit : One person can be DEO of one District for atmost 8 years in a District in his lifetime. After that, he may become DEO of any other District or may serve on any other position in that District.
.
(12) No retirement benefits : if a person has become DEO via approval of parents, and is not part of Govt cadre, then he will not be eligible for any retirement benefits after his term ends due to term limit or expulsion. If any other law or resolution passed by District / State / India may provide him retirement benefits. But this law doesn’t create any provision of any retirement benefits.
.
(13) Salary of DEO brought in by approval of parents : Salary and perks of DEO brought in by approval of parents will be same as median salary of District Collectors in the Districts of the State irrespective of seniority and other considerations. DEO will not be eligible for any government housing or personal staff at home or government car or car driver, but he will get equivalent monetary amount. Decision of CM on this issue will be final. But all DEOs brought in by approvals of parents must get same salary from the State Government. And parents of District thru explicit majority may give additional salary to a DEO.
.
(14) How can CM appoint person approved by voters as DEO : The decision will be left to CM. CM may appoint him as OSD (OSD = Officer on Special Duty) and then appoint him as DEO. The decision of CM will be final 
.
(S) Suspension of RTR-DEO by people of State or of people of India
.
(S.1) The voters (all voters, not just parents) of a State can suspend this RTR DEO procedure in a District, for 4 years, using a procedure, established by CM. The procedure should show that explicit majority of ALL voters (not just voters who voted or said YES / NO) in the state have approved the suspension. And suspension can be extended by voters for 4 more years, several times. 
.
(S.2) The voters (all voters, not just parents) of India can suspend this RTR DEO procedure in a District or a State, for 4 years, using a procedure, established by PM. The procedure should show that explicit majority of ALL voters (not just voters who voted or said YES / NO) in India have approved the suspension. And suspension can be extended by voters for 4 more years, several times.
.
(S.3) comment : “S” stands for sovereignty. Any District level procedure can be suspended by explicit majority voters of State / India. And any State level procedure can be suspended by explicit majority voters of India. 
.
(RV) Non binding directive for Range Voting :
A few weeks or months after this law has passed, ECI may or need not allow voters to submit marks between (-100 to 100) for each candidate or just submit approvals. If voter is submitting approval and not marks, then no approval will mean 0 marks and approval will mean 100 marks. But if voter is giving marks, then actual marks given by him will be counted. The candidates total marks will be sum of all marks he got. And if candidate has 1 or more marks from over 51% of all voters, then system will ignore all his negative marks. The total marks of sitting MP will be higher of the two ---- [ (votes he got * 67) , score he got after election ] . And recall poll will happen when a candidates score is (MP’s score + voter population * 100 / 10). And recall will happen as per clause-15. ECI may or need not implement this clauses, only after over 51% of all voters of India has specified YES using clause-CV.2. This procedure is called as Range Voting, and it is immune to so called AUIT (= Arrow’s Useless Impossibility Theorem) . Range Voting is better than Approval Voting used here. 
.
(CV) Citizens’ Voice:
.
(CV.1) If any citizen residing anywhere in India wants any change in this law or has any complaint under this law, then he may submit an affidavit to DC’s designated clerk and clerk will scan the affidavit on put on CM’s website along with photo and details of the citizen. The clerk will charge Rs 30 per page. The clerk will issue serial number for the affidavit.
.
(CV.2) If any citizen residing in the area covered by the Patwari wants to register his YES / NO on the affidavit submitted in above clause, then he can go to Talati’s office and provide the serial number of the affidavit and his YES/NO on a form, and Patwari will enter his YES/NO for a fee for Rs 3 . The YES/NO shall come on CM’s website along with citizen’s voter-id, name, date etc
.
-------------- end of Right to Recall DEO draft ------

[ टिप्पणी : राईट टू रिकॉल ज़िला शिक्षा अधिकारी का यह कानूनी ड्राफ्ट मुख्यमंत्री द्वारा सीधा राज्य के राजपत्र ( गैजेट ) में छापा जा सकता है, मुख्यमंत्री को विधानसभा या लोकसभा से इस प्रस्ताव को पारित करवाने की आवश्यकता नहीं है। 
.
इस ड्राफ्ट के तीन भाग है -
.
भाग - I : नागरिकों के लिए सामान्य निर्देश
.
भाग- II : ड्राफ्ट में दी गयी प्रक्रिया का सारांश
.
भाग- III : अधिकारियों के लिए निर्देश एवं इस सम्बन्ध में टिप्पणियाँ 
.
इस ड्राफ्ट के महत्वपूर्ण खंड है - (1.2) , (1.3) , (1.5) , (8) , (10)
इस कानून-ड्राफ्ट के लेखको की राय में अन्य खंड अति-महत्वपूर्ण नही है और केवल सामान्य समझ के लिए है। ]
.
----
.
--------------------------------------------------
भाग - I : नागरिकों के लिए सामान्य निर्देश
--------------------------------------------------
.
(1) पदासीन जिला शिक्षा अधिकारी को या नये ज़िला शिक्षा अधिकारी की उम्मीदवारी को अभिभावकों द्वारा अनुमोदन या स्वीकृति दर्ज करना :
.
(1.1) यदि कोई अभिभावक (अभिभावक से आशय ऐसे मतदाता से है जिसकी संतान की उम्र 17 वर्ष से कम है ) अपने जिले के शिक्षा अधिकारी को बदलना चाहता है, तो वह पटवारी कार्यालय में किसी भी दिन उपस्थित होकर किसी उम्मीदवार को अपना अनुमोदन दे सकता है। अनुमोदन दर्ज करवाने की इस प्रक्रिया के लिए समय , दिनांक , अवधि आदि का कोई निर्बन्धन लागू नहीं होगा। 
.
(1.2) कोई भी अभिभावक पटवारी कार्यालय में उपस्थित होकर 3 रू शुल्क अदा करके शिक्षा अधिकारी पद के लिए अधिक से अधिक पांच व्यक्तियों को अनुमोदित कर सकेगा। यदि अभिभावक पदासीन शिक्षा अधिकारी को अनुमोदित करना चाहता है तो वह निशुल्क ऐसा कर सकेगा। इन अनुमोदनों को मुख्यमंत्री की वेबसाइट पर रखा जाएगा तथा ये अनुमोदन सभी के लिए खुले एवं दृश्य होंगे। 
. 
(1.2.a) बीपीएल कार्ड धारको के लिए शुल्क 1 रूपये होगा।
.
(1.2.b) टिपण्णी : यदि कोई अभिभावक पदासीन शिक्षा अधिकारी को अनुमोदित करता है तो वह अधिकतम 4 अन्य उम्मीदवारों को अनुमोदित कर सकेगा। यदि अभिभावक पदासीन शिक्षा अधिकारी को अनुमोदित नहीं करता है तब वह 5 अन्य उम्मीदवारों को अनुमोदित कर सकेगा। 
. 
(1.3) अभिभावक पटवारी कार्यालय में किसी भी दिन उपस्थित होकर किसी भी उम्मीदवार के अनुमोदन को रद्द कर सकेगा। 
.
(1.4) या तो कोई अभिभावक किसी उम्मीदवार को अनुमोदित कर सकता है या नहीं कर सकता है --- इन अनुमोदनों का कोई वरीय क्रम (ranking ) नही होगा। 
.
(1.5) ये अनुमोदन तब तक वैध रहेंगे जब तक कि अनुमोदनकर्ता स्वयं इन्हे रद्द न कर दे या मतदाता का नाम मतदाता सूची में से हटा ना दिया जाए। 
.
(1.6) यदि मुख्यमंत्री ऐसी प्रकिया उपलब्ध करवाते है, तो अभिभावक अपना अनुमोदन बैंक एटीएम, एस एम एस , मोबाईल ऐप, या इंटरनेट द्वारा दर्ज / रद्द करवा सकेंगे।
.
(2) सीमांकित मतदान या रेंज वोटिंग के लिए बाद में जोड़े जा सकने वाले अबाध्यकारी निर्देश :
.
इस विधेयक के लागू होने के बाद मुख्यमंत्री ऐसी प्रक्रिया लागू कर सकेंगे जिससे मतदाता किसी उम्मीदवार को -100 से 100 ( -100 to +100 ) के बीच अंक दे सके। ऐसी प्रक्रिया लागू होने के बाद मतदाता उम्मीदवार को अंक दे सकेंगे। यदि मतदाता उम्मीदवार को कोई अंक नहीं देता है तो इसे शून्य अंक माना जाएगा। यदि मतदाता सीमांकित मतदान प्रणाली को जटिल पाते है तो वे सामान्य अनुमोदन की प्रक्रिया जारी रख सकते है। 
.
-------------------------------------------
भाग-II: ड्राफ्ट में दी गई प्रक्रिया का सार
------------------------------------------- 
.
(3) टिप्पणी: सम्पूर्ण ड्राफ्ट का सार 
. 
(3.1) कोई भी नागरिक किसी भी दिन अधिकतम 5 उम्मीदवारों को अनुमोदित कर सकता है एवं पदासीन जिला शिक्षा अधिकारी को भी अनुमोदित कर सकता और किसी भी दिन अपना अनुमोदन निरस्त कर सकता है। 
. 
(3.2) जब किसी उम्मीदवार के अनुमोदनो की संख्या कार्यरत जिला शिक्षा अधिकारी के अनुमोदनो की संख्या से अधिक होगी तथा उस उम्मीदवार के अनुमोदनो की संख्या अमुक ज़िले में दर्ज सभी अभिभावकों की संख्या से 35% से अधिक भी होगी, और यह आधिक्य कार्यरत जिला शिक्षा अधिकारी के अनुमोदनो से 2% अधिक भी होगा, तब मुख्यमंत्री ऐसे उम्मीदवार को नया जिला शिक्षा अधिकारी नियुक्त कर सकते है, या उन्हें ऐसा करने की आवयश्कता नही है। इस सम्बन्ध में मुख्यमंत्री का निर्णय अंतिम माना जायेगा। 
. 
(3.3) किसी राज्य के मतदाता अमुक राज्य में दर्ज कुल मतदाताओं के स्पष्ट बहुमत का प्रदर्शन करके अमुक राज्य में स्थित किसी जिले में लागू राईट टू रिकॉल जिला शिक्षा अधिकारी की इस प्रक्रिया को 4 साल तक के लिये निरस्त कर सकते है। और भारत देश के मतदाता देश में दर्ज कुल मतदाताओं के स्पष्ट बहुमत का प्रदर्शन करके किसी राज्य या जिले में लागू राईट टू रिकॉल जिला शिक्षा अधिकारी की इस प्रक्रिया को 4 साल तक के लिये निरस्त कर सकते है।
. 
--------------------------------------------------------
भाग III : अधिकारीयों के लिए निर्देश और टिप्पणीयां 
--------------------------------------------------------
.
(4) टिप्पणी क्या है : ये टिप्पणीयां इस कानूनी ड्राफ्ट का वास्तविक हिस्सा नहीं है। कार्यकर्ता और नागरिक ड्राफ्ट को समझने हेतु इसका उपयोग कर सकते है। जूरी सदस्य इन टिप्पणीयों का उपयोग किसी मामले के फैसले के लिए सन्दर्भ के रूप में ले सकते है। किन्तु ये टिप्पणीयां किसी पर भी बाध्यकारी नहीं है। 
.
(5) (5.1) अभिभावक शब्द का आशय यहाँ ऐसे नागरिक से है जो किसी ज़िले में पंजीकृत मतदाता है तथा जिसकी कम से कम एक संतान है तथा इस संतान की आयु 0 से 17 वर्ष के मध्य है। राईट टू रिकॉल जिला शिक्षा अधिकारी प्रक्रिया में सिर्फ अभिभावक ही अनुमोदन दर्ज कर सकेंगे। 
.
(5.2) जिला कलेक्टर ज़िले के अभिभावकों की सूची मुख्यमंत्री द्वारा दिए गए निर्देशों के आधार पर बनाएगा और इन सूचियों का रख रखाव करेगा। जब तक सूची नहीं बनती, 23 वर्ष से 45 वर्ष की आयु के मध्य का कोई भी मतदाता अभिभावक माना जायेगा। 
.
(5.3) शब्द "कर सकते है" का आशय है - किसी भी प्रकार का नैतिक-कानूनी बंधन नहीं। इसका स्पष्ट अर्थ है "कर सकते है" या "ऐसा करने की आवश्यकता नहीं है"। 
.
(6) जिला शिक्षा अधिकारी के लिए उम्मीदवारी
.
(6.1) यदि किसी व्यक्ति के पास स्नातक डिग्री है तथा वह लोकसभा चुनाव लड़ने के लिए आवश्यक पात्रता पूरी करता है, तो वह जिला शिक्षा अधिकारी बनने के लिए आवेदन कर सकता है। यदि इन पात्रताओ को धारण करने वाला कोई व्यक्ति जिला कलेक्टर के समक्ष आता है और कार्यरत जिला शिक्षा अधिकारी को प्रतिस्थापित करने के लिए स्वयं को पंजीकृत करने हेतु आवेदन करता है तो जिला कलेक्टर सांसद के चुनाव में ली जाने वाली राशि के बराबर शुल्क लेकर उसका आवेदन स्वीकार करेगा। 
.
(6.2) जिला शिक्षा अधिकारी के लिए प्रस्तुत आवेदन लोकसभा के लिए प्रस्तुत किये जाने वाले आवेदन के सदृश होगा। जिला कलेक्टर 7 दिन के अन्दर आवेदन को सत्यापित करके स्वीकृत या अस्वीकृत करेगा। 
.
(6.3) यदि आवेदन अस्वीकृत होता है तो 2000 रूपये की राशि जब्त करके शेष जमा राशि वापस लौटा दी जाएगी। यदि उम्मीदवार अपना आवेदन पुन: प्रस्तुत करना चाहता है तो वह ऐसा कर सकता है। 
.
(6.4) आवेदन स्वीकृत होने पर जिला कलेक्टर एक सीरियल नंबर जारी करेगा और प्रतिस्थापक उम्मीदवार का नाम और अन्य सम्बंधित सूचना मुख्यमंत्री की वेबसाइट पर रखेगा। यदि उम्मीदवार चाहे तो लिखित में आवेदन करके अपनी उम्मीदवारी वापिस ले सकते है। 
.
(6.5) जिला कलेक्टर कार्यरत जिला शिक्षा अधिकारी को बिना कोई शुल्क लिए उम्मीदवार के रूप में पंजीकृत करेगा। उम्मीदवारों की सूची में कार्यरत जिला शिक्षा अधिकारी का सीरियल नंबर 1 होगा।
.
(7) नागरिको द्वारा प्रतिस्थापित उम्मीदवार या कार्यरत जिला शिक्षा अधिकारी को अनुमोदित करना
.
(7.1) यदि कोई अभिभावक पटवारी कार्यालय आकर मुख्यमंत्री द्वारा निर्धारित शुल्क देता है, और अधिकतम पांच उम्मीदवारों को जिला शिक्षा अधिकारी के पद के लिए अनुमोदित करता है और/ या वह कार्यरत जिला शिक्षा अधिकारी को अनुमोदित करता है तो पटवारी उसकी स्वीकृति को कंप्यूटर में दर्ज करेगा और बदले में उसे रसीद देगा। इस रसीद में नागरिक की मतदाता पहचान पत्र संख्या, दिनांक / समय और उम्मीदवार जिन्हे उसने अनुमोदित किया है दर्ज होंगे।
.
(7.2) यदि कोई अभिभावक अपना अनुमोदन निरस्त करने आता है तो पटवारी उसके एक या उससे अधिक अनुमोदन बिना कोई शुल्क लिए निरस्त कर देगा। इस निरस्तीकरण को भी मुख्यमंत्री की वेबसाइट पर रखा जाएगा। 
.
(7.3) टिप्पणी : अनुमोदनों के बारे में स्पष्टीकरण - (i) अभिभावक किसी भी दिन अनुमोदन दर्ज कर सकता है और इसके लिए कोई समय सीमा नहीं है। अपनी सुविधानुसार विभिन्न अभिभावक अपने अनुमोदन किसी भी दिन दर्ज कर सकते है। (ii) मान लीजिये, एक अभिभावक ने उम्मीदवार A, B और C को अनुमोदित किया है। तब A, B और C की अनुमोदन संख्या 1 से बढ़ जाएगी। (iii) अब अभिभावक किसी भी दिन अपना कोई भी अनुमोदन निरस्त कर सकता है। मान लीजिये कि अमुक अभिभावक ने C के अनुमोदन को निरस्त किया है, तो C की अनुमोदन संख्या 1 से कम हो जाएगी और A और B के अनुमोदनों की संख्या अपरिवर्तित रहेगी। (iv) अभिभावक किसी अन्य उम्मीदवार को भी किसी भी दिन अनुमोदित कर सकता है। मान लीजिये कि अमुक अभिभावक D को अनुमोदित करता है, तो ऐसी स्तिथि में D की अनुमोदन संख्या 1 से बढ़ जाएगी और A और B की अनुमोदन संख्या अपरिवर्तित रहेगी। 
.
(7.4) पटवारी प्रत्येक अभिभावक द्वारा दर्ज अनुमोदन को अभिभावक की मतदाता संख्या, नाम और उसके द्वारा अनुमोदित उम्मीदवारों के नाम के साथ मुख्यमंत्री की वेबसाइट पर रखेगा। 
.
(7.5) मुख्यमंत्री ऐसा सिस्टम बना सकते है जिससे अभिभावक को उनके द्वारा अनुमोदित किये गए उम्मीदवारों के सम्बन्ध में SMS से सूचना दी जा सके। 
.
(7.5.a) टिप्पणी : ऊपर बताया गया SMS, ऐसे SMS के समान हो सकता है जो किसी को क्रेडिट कार्ड से लेनदेन करने पर प्राप्त होता है। तो यदि कोई व्यक्ति किसी अभिभावक के साथ धोखाधड़ी कर रहा है, तब अभिभावक इस सम्बन्ध में तुरंत जान जाएगा और शिकायत दर्ज करवा सकेगा। फिंगर प्रिंट और फोटो आदि के होने से धोखाधड़ी की सम्भावनाओ में और भी कमी आएगी।
. 
(7.6) मुख्यमंत्री पटवारी को अभिभावक के फिंगर प्रिंट और फोटो लेने वाले उपकरण उपलब्ध करवा सकते है तथा ऐसी रसीद उपलब्ध करवा सकते है, जिसमे अभिभावक के फिंगर प्रिंट और फोटो हो। 
.
(7.7) मुख्यमंत्री एक ऐसा सिस्टम बना सकते है जहाँ अभिभावक पटवारी कार्यालय में रखी ATM जैसी मशीनों पर अपने पंजीकृत मोबाइल से SMS भेजकर या इन्टरनेट द्वारा या मोबाइल फ़ोन एप्प द्वारा अपनी स्वीकृति दर्ज करवा सके। मुख्यमंत्री इसके लिए शुल्क निर्धारित कर सकेंगे। मुख्यमंत्री बैंकों के साथ ऐसी व्यवस्था रच सकते है जिससे मौजूदा ATM मशीनों द्वारा भी अभिभावक अपने अनुमोदन दर्ज कर सके।
.
(7.8) अनुमोदन दर्ज करने की प्रक्रिया खुली और सार्वजानिक होगी। अर्थात प्रत्येक मतदाता द्वारा दर्ज किये गए अनुमोदनों को मुख्यमंत्री की वेबसाइट पर रखा जाएगा और यह प्रत्येक व्यक्ति के लिए खुली एवं दृश्य होगी। 
.
(7.9) नागरिको द्वारा दर्ज कोई अनुमोदन तब तक वैध होगा जब तक अभिभावक इस अनुमोदन को स्वयं बदल नही देता या अभिभावक का नाम मतदाता सूची में से नहीं हटा दिया जाता। इस तरह उम्मीदवार के अनुमोदनों की संख्या तब बढ़ जायेगी जब अभिभावक अनुमोदन दर्ज करेगा और यह संख्या सिर्फ तब घटेगी जब अभिभावक अपना अनुमोदन निरस्त करेगा या जब अभिभावक का नाम मतदाता सूची में से हटा दिया जाएगा।
. 
(8) जिला शिक्षा अधिकारी के बदले जाने के लिए शर्त : यदि
(8.1) किसी नए उम्मीदवार को प्राप्त अनुमोदनों की संख्या अमुक जिले के अभिभावकों की कुल संख्या के 35% (सभी दर्ज अभिभावकों की कुल संख्या के 35%) से अधिक हो, और
(8.2) उसे मिले अनुमोदनों की संख्या पदासीन जिला शिक्षा अधिकारी को मिले अनुमोदनों की संख्या से उतनी अधिक है जो उस जिले में दर्ज अभिभावको की कुल संख्या के 2% के बराबर हो। और
(8.3) उसके अनुमोदनों की संख्या सबसे अधिक हो।
तभी और सिर्फ तभी मुख्यमंत्री पदासीन जिला शिक्षा अधिकारी को पद से हटा सकते है और ऊपर दी गयी शर्ते पूरी करने वाले नए उम्मीदवार को जिला शिक्षा अधिकारी नियुक्त कर सकते है, या उन्हें ऐसा करने की ज़रूरत नहीं है। 
. 
(9) टिपण्णी :
. 
(9.1) ऊपर दी गयी धाराओं में इंगित "35%" और "2%" से आशय है "अभिभावकों की मतदाता सूचि में दर्ज कुल अभिभावकों की संख्या के 35% और 2% "। उदाहरण : यदि किसी जिले में 20 लाख अभिभावक है तो 35% और 2% का अर्थ क्रमशः 700,000 और 40,000 होगा। यहाँ यह संख्या अनुमोदन दर्ज कराने वाले अभिभावकों की संख्या से अप्रभावित रहेगी। और प्रतिस्थापन के लिए ऊपर दी गयी तीनो धाराओं (8.1) ,(8.2) ,(8.3) में दी गयी शर्तो का पूरा होना जरुरी होगा। 
.
(9.2) उदहारण – 1 : मान लीजिए किसी जिले में 20,00,000 अभिभावक है ,तथा मान लीजिए कि पदासीन जिला शिक्षा अधिकारी को 500,000 अभिभावकों के अनुमोदन मिले है। तब यदि किसी उम्मीदवार को 20,00,000 के 35% = 700,000 अभिभावकों के अनुमोदन मिलते है, सिर्फ तभी मुख्यमंत्री उस नए उम्मीदवार को जिला शिक्षा अधिकारी नियुक्त कर सकते है या उन्हें ऐसा करने की ज़रूरत नहीं है। 
.
(9.3) उदहारण – 2 : मान लीजिए किसी जिले में 20,00,000 अभिभावक है तथा मान लीजिए कि पदासीन जिला शिक्षा अधिकारी के पास 800,000 अभिभावकों का अनुमोदन है। तब किसी उम्मीदवार के अनुमोदनों की संख्या (800,000 + 2% of 20,00,000) = (800,000 + 40,000) = 840,000, होना जरुरी है। सिर्फ तभी मुख्यमंत्री उसे जिला शिक्षा अधिकारी नियुक्त कर सकेंगे या उन्हें ऐसा करने की ज़रूरत नहीं है। 
.
10) कोई नागरिक किसी राज्य में अधिकतम 5 जिलों का और पुरे भारत में अधिकतम 15 जिलों का जिला शिक्षा अधिकारी बन सकता है। यदि कोई जिला शिक्षा अधिकारी अभिभावकों के अनुमोदन द्वारा शिक्षा अधिकारी बना है अर्थात प्रत्येक जिले में उसे 35% अभिभावकों से ज्यादा अभिभावकों के समर्थन प्राप्त है , तो उसका वेतन एवं भत्ते एक जिला शिक्षा अधिकारी के वेतन एवं भत्तों से उतना गुना होगा जितने जिलो में वह जिला शिक्षा अधिकारी के पद पर कार्यरत है। उदाहरण के लिए- यदि कोई व्यक्ति भारत के 7 जिलों में शिक्षा अधिकारी है, और सिर्फ 5 जिलों में ही उसको मिले अनुमोदनों की संख्या 35% से ज्यादा है तो उसे पांच गुणा वेतन मिलेगा न कि सात गुणा। किन्तु यदि कोई व्यक्ति 5 जिलो में जिला शिक्षा अधिकारी है एवं 4 या 5 जिलों में प्रत्येक जिले से उसे मिले अनुमोदनों की संख्या 35% से कम है, तब उसका वेतन सिर्फ एक जिला शिक्षा अधिकारी के वेतनमान के बराबर होगा। 
.
(11) कार्य अवधि: कोई व्यक्ति अपने जीवनकाल में एक जिले में अधिकतम 8 साल तक जिला शिक्षा अधिकारी रह सकता है। इसके बाद वह किसी अन्य जिले में जिला शिक्षा अधिकारी बन सकता है या अमुक जिले में किसी अन्य पद पर सेवा दे सकता है। 
.
(12) कोई भी सेवानिवृत्ति लाभ नहीं : यदि कोई व्यक्ति अभिभावकों के समर्थन द्वारा जिला शिक्षा अधिकारी बना है और वह किसी भी राजकीय सेवा कैडर से नहीं आता है तो कार्य अवधी समाप्त होने पर या हटा दिये जाने पर उसे किसी भी प्रकार की सेवानिवृत्ति राशी एवं पेंशन लाभ आदि नहीं मिलेंगे। जिला / राज्य / भारत सरकार द्वारा पारित किये गए किसी कानून या प्रस्ताव द्वारा उसे सेवानिवृति लाभ दिए जा सकेंगे परन्तु यह कानून शिक्षा अधिकारी को किसी भी प्रकार के सेवानिवृत्ति लाभ के लिए कोई प्रावधान नहीं करता।
.
(13) अभिभावकों द्वारा अनुमोदित शिक्षा अधिकारी का वेतनमान एवं भत्ते : अभिभावकों द्वारा अनुमोदित शिक्षा अधिकारी का वेतन एवं भत्ते वरिष्ठता एवं अन्य पात्रताओं की अवहेलना करते हुए राज्य सरकार द्वारा नियुक्त किये जाने वाले जिला कलेक्टर के वेतनमान के समकक्ष होंगे। शिक्षा अधिकारी सरकारी आवास, सरकारी वाहन, ड्राइवर या अपने आवास पर प्राप्त की जाने वाली सेवाओं की पूर्ती के लिए किसी भी प्रकार के अतिरिक्त स्टाफ का पात्र नहीं होगा। किन्तु जिला शिक्षा अधिकारी इन सेवाओं के बराबर निर्धारित राशि भत्ते के रूप में प्राप्त कर करेगा। इस सम्बन्ध में मुख्यमंत्री का फैसला अंतिम होगा। किन्तु अभिभावकों के अनुमोदन से नियुक्त सभी शिक्षा अधिकारी राज्य सरकार द्वारा निर्धारित वेतनमान के समान वेतन प्राप्त करेंगे। यदि जिले के अभिभावक चाहे तो वे बहुमत का प्रदर्शन करके शिक्षा अधिकारी के वेतन में अतिरिक्त वृध्दि कर सकेंगे। 
.
(14) अभिभावकों द्वारा अनुमोदित व्यक्ति को मुख्यमंत्री द्वारा शिक्षा अधिकारी नियुक्त करना : इस सम्बन्ध में फैसला मुख्यमंत्री करेंगे। मुख्यमंत्री अभिभावकों द्वारा अनुमोदित व्यक्ति को ओएसडी (OSD = Officer on Special Duty) के रूप में या शिक्षा अधिकारी के रूप में नियुक्ति दे सकते है। मुख्यमंत्री का फैसला अंतिम होगा।
. 
(S) देश अथवा राज्य के नागरिको द्वारा शिक्षा अधिकारी को निलंबित करने की प्रक्रिया : 
.
(S.1) राज्य के मतदाता ( सभी मतदाता, न कि सिर्फ अभिभावक मतदाता ) मुख्यमंत्री द्वारा स्थापित की गयी प्रक्रिया का उपयोग करते हुए इस कानून को किसी जिले / राज्य में 4 वर्ष तक के लिए निलंबित कर सकते है। इस प्रक्रिया के दौरान यह सुनिश्चित होना चाहिए कि अमुक राज्य के कुल मतदाताओं ( न कि सिर्फ उन मतदाताओं ने जिन्होंने अनुमोदन की प्रक्रिया में भाग लिया है ) का बहुमत स्पष्ट रूप से इस निलंबन का समर्थन करता है। साथ ही राज्य के मतदाता अपने बहुमत का प्रयोग करते हुए इस निलंबन को 4 वर्षो की अवधि में असीमित आवृतियों तक बढ़ा सकेंगे। 
.
(S.2) देश के मतदाता ( सभी मतदाता, न कि सिर्फ अभिभावक मतदाता ) प्रधानमंत्री द्वारा स्थापित की गयी प्रक्रिया का उपयोग करते हुए इस कानून को किसी जिले / राज्य में 4 वर्ष तक के लिए निलंबित कर सकते है। इस प्रक्रिया के दौरान यह सुनिश्चित होना चाहिए कि देश के कुल मतदाताओं ( न कि सिर्फ उन मतदाताओं ने जिन्होंने अनुमोदन की प्रक्रिया में भाग लिया है ) का बहुमत स्पष्ट रूप से इस निलंबन का समर्थन करता है। साथ ही देश के मतदाता अपने बहुमत का प्रयोग करते हुए इस निलंबन को 4 वर्षो की अवधि के लिए असीमित आवृतियों तक बढ़ा सकेंगे। 
.
(S.3) टिपण्णी : यहाँ "S" सम्प्रभुता ( Sovereignty ) का द्योतक है। आशय यह है कि, किसी भी जिले में लागू प्रक्रिया को राज्य / देश के मतदाता बहुमत का प्रदर्शन करके निलंबित कर सकते है, तथा किसी जिले / राज्य में लागू प्रक्रिया को देश के मतदाताओं का बहुमत निलंबित कर सकता है। 
.
(RV) [ सीमांकित मतदान या रेंज वोटिंग के लिए टिप्पणी ] 
.
अबाध्यकारी निर्देश - इस विधेयक के लागू होने के बाद चुनाव आयोग मतदाताओ को यह अनुमति दे सकेगा कि वे किसी उम्मीदवार को 0 से 100 के बीच अंक दे सके या फिर सामान्य रूप से हाँ / नहीं का अनुमोदन कर सके। यदि मतदाता द्वारा सामान्य अनुमोदन किया जाता है तो इस अनुमोदन को 100 अंको के समकक्ष माना जाएगा। किन्तु यदि मतदाता उम्मीदवार को अंक देता है तो मतदाता द्वारा दिए गए अंक ही मान्य होंगे। उम्मीदवार द्वारा प्राप्त किये गए सभी अंक ही उम्मीदवार के कुल अंको की संख्या मानी जायेगी। पदासीन अधिकारी के कुल अंको की मान्य संख्या दी गयी दोनों परिस्थितियों में से उच्च अंक को निर्दिष्ट करने वाली संख्या को माना जाएगा -- (अ) पदासीन अधिकारी को प्राप्त कुल मत * 67, (ब) चुनाव के बाद पदासीन अधिकारी द्वारा प्राप्त अंक।
प्रतिस्थापक चुनाव तब आयोजित किया जाएगा जब किसी उम्मीदवार के कुल अंको की संख्या ( पदासीन अधिकारी के कुल अंको की संख्या + अमुक निर्वाचन क्षेत्र में दर्ज कुल मतदाताओ की संख्या * 100 / 10 ) के बराबर हो। तथा पदासीन अधिकारी का प्रतिस्थापन तब होगा जब उम्मीदवार को पदासीन अधिकारी से अधिक वोट मिलते है , और नए उम्मीदवार को पदासीन अधिकारी को पिछले चुनावों में मिले वोटो से 10% अधिक वोट मिलते है, या फिर नए उम्मीदवार को निर्वाचन क्षेत्र में दर्ज कुल मतदाताओ के 51% मतों से ज्यादा मिलते है। इस विधेयक की धारा ( CV.1 ) का उपयोग करते हुए यदि देश के कुल मतदाताओं के 51% नागरिक इस व्यवस्था को लागू करने के "हाँ" दर्ज कर देते है तो चुनाव आयोग इस प्रणाली को लागू कर सकता है या नही भी कर सकता है। यह कथित सीमांकित मतदान प्रणाली या रेंज वोटिंग सिस्टम ऐरो की कथित असम्भाव्यता प्रमेय ( Arrow’s Impossibility Theorem ) से प्रतिरक्षा प्रदान करती है।
.
(CV) जनता की आवाज (Citizens’ Voice):
. 
(CV.1 ) [ जिला कलेक्टर या उसके द्वारा नियुक्त कर्मचारी के लिए निर्देश ]
. 
यदि देश के किसी भी क्षेत्र में निवास करने वाला कोई भी नागरिक मतदाता इस विधेयक के किसी अनुच्छेद में बदलाव चाहता हो अथवा वह इस विधेयक से सम्बन्धित कोई शिकायत दर्ज करना चाहता हो तो ऐसा नागरिक मतदाता जिला कलेक्टर के कार्यालय में उपस्थित होकर एक शपथपत्र प्रस्तुत कर सकता है। जिला कलेक्टर या उसका क्लर्क इस शपथपत्र को 30 रूपए प्रति पृष्ठ का शुल्क लेकर दर्ज करेगा तथा एक सीरियल नंबर जारी करके इस शपथपत्र को स्कैन करके अमुक मतदाता के छाया चित्र (फोटो) एवं अन्य विवरण के साथ मुख्यमंत्री की वेबसाईट पर रखेगा।
. 
(CV.2 ) [ तलाटी अथवा पटवारी के लिए निर्देश ]
. 
किसी पटवारी कार्यालय के कार्यक्षेत्र में निवास करने वाला कोई भी नागरिक मतदाता यदि इस विधेयक अथवा इसके किसी अनुच्छेद पर अपनी आपत्ति दर्ज कराना चाहता हो अथवा ऊपर दी गयी धारा (CV.1 ) के तहत प्रस्तुत किसी भी शपथपत्र पर अपनी हां / नहीं दर्ज कराना चाहता हो, तो वह अपना मतदाता पहचान पत्र लेकर तलाटी के कार्यालय में उपस्थित होगा और उपलब्ध आवेदन पर शपथपत्र का सीरियल नंबर दर्ज करके अपनी हाँ / नही दर्ज करेगा। पटवारी 3 रूपए का शुल्क लेकर इस हाँ / नहीं को दर्ज करके एक रसीद जारी करेगा । मतदाता द्वारा दर्ज की गयी हाँ / नहीं को मुख्यमंत्री की वेबसाइट पर मतदाता के नाम, मतदाता पहचान संख्या, दिनांक एवं अन्य विवरणों के साथ रखा जाएगा। 
. 
==== जिला शिक्षा अधिकारी को प्रतिस्थापितके प्रस्तावित ड्राफ्ट का समापन ====